import requests
from pymongo import MongoClient
"""url = 'https://newsapi.org/v1/articles?source=the-verge&sortBy=top&apiKey=2806bca394a346aa931291650f78bba9'
response = requests.get(url)
result=response.json()
print(result['articles'])"""

mongo= MongoClient('mongodb://newsuser:newsuser123@ds251435.mlab.com:51435/news')
db= mongo['news']
articlesCollection= db['articles']
count=0

newsList=["the-wall-street-journal", "bbc-news", "the-hindu", "the-times-of-india", "the-economist", "the-washington-post","cnn","the-new-york-times","the-verge","techradar", "national-geographic", "ign", "entertainment-weekly", "reddit-r-all", "new-scientist", "techcrunch", "bbc-sport", "espn", "the-sport-bible", "fox-sports"]

def newsScraping(newsKey):
	try:
		url = 'https://newsapi.org/v1/articles?source='+newsKey+'&sortBy=top&apiKey=2806bca394a346aa931291650f78bba9'
		response = requests.get(url)
		result=response.json()
		if result is not None:
			articles=result['articles']
			findandInsertData(newsKey,articles)
	except requests.exceptions.RequestException as e:
		print(e)

def findandInsertData(newsSource,data):
	for news in data:
		global articlesCollection
		status= articlesCollection.update_one(
			{"title": news['title']},
			{"$setOnInsert":
				{
					"title":news['title'] if news['title'] is not None else "",
					"description":news['description'] if news['description'] is not None else "",
					"url":news['url'] if news['url'] is not None else "",
					"urlToImage":news['urlToImage'] if news['urlToImage'] is not None else "",
					"publishedAt":news['publishedAt'] if news['publishedAt'] is not None else "",
					"author":news['author'] if news['author'] is not None else "",
					"source":newsSource}
				},upsert=True)
		if status.modified_count == 1:
			global count
			count =count+1

for newsK in newsList:
	newsScraping(newsK)
	print(count)


